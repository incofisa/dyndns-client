#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_icon=dns-setting.ico
#AutoIt3Wrapper_UseUpx=n
#AutoIt3Wrapper_Res_Comment=Actualitzador de DynDNS, per Sime� Reig
#AutoIt3Wrapper_Run_Obfuscator=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <Constants.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>

#include <Inet.au3>
#include "WinHttp.au3"

Opt("GUIOnEventMode", 1)

Opt("TrayAutoPause", 0) ; no es pot pausar
Opt("TrayOnEventMode", 1)
Opt("TrayIconHide", 0) ; no amagar l'icona
Opt("TrayMenuMode", 1) ; sense menu al trayIcon
TraySetIcon(@ScriptDir & "\dns-setting.ico")

TraySetOnEvent($TRAY_EVENT_PRIMARYUP, "On_Click_TrayIcon")


#Region ### START Koda GUI section ### Form=D:\Dropbox\autoIT\KODA\DynDNS.kxf
$Form_Principal = GUICreate("Actualiza DNS", 241, 190, -1, -1)
GUISetIcon(@ScriptDir & "\dns-setting.ico", -1)
GUISetOnEvent($GUI_EVENT_CLOSE, "Form_PrincipalClose")
GUISetOnEvent($GUI_EVENT_MINIMIZE, "Form_PrincipalMinimize")
GUISetOnEvent($GUI_EVENT_MAXIMIZE, "Form_PrincipalMaximize")
GUISetOnEvent($GUI_EVENT_RESTORE, "Form_PrincipalRestore")
$Group_DNS = GUICtrlCreateGroup(" Dades DynDNS ", 8, 0, 225, 185)
GUICtrlSetFont($Group_DNS, 8, 800, 0, "MS Sans Serif")
$botoActualitza = GUICtrlCreateButton("Actualitza DNS", 15, 147, 209, 33)
GUICtrlSetOnEvent($botoActualitza, "botoActualitzaClick")

$input_UserDynDNS = GUICtrlCreateInput("", 80, 24, 145, 21)
$input_PassDynDNS = GUICtrlCreateInput("", 80, 49, 145, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_PASSWORD))
$input_HostDynDNS = GUICtrlCreateInput("", 80, 73, 145, 21)
$Input_Actual = GUICtrlCreateInput("", 80, 98, 145, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_LEFT))
$Input_IP = GUICtrlCreateInput("", 80, 122, 145, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_LEFT))

GUICtrlSetBkColor($input_UserDynDNS, 0xFFFFE1)
GUICtrlSetBkColor($input_PassDynDNS, 0xFFFFE1)
GUICtrlSetBkColor($input_HostDynDNS, 0xFFFFE1)

GUICtrlSetTip($input_UserDynDNS, "User a DynDNS")
GUICtrlSetTip($input_PassDynDNS, "Password a DynDNS")
GUICtrlSetTip($input_HostDynDNS, "Host a DynDNS que volem canviar la IP")
GUICtrlSetTip($Input_Actual, "IP que t� actualment el Host de dynDNS")
GUICtrlSetTip($Input_IP, "IP P�blica nostra actual")


$Label2 = GUICtrlCreateLabel("Usuari", 18, 29, 34, 17)
$Label3 = GUICtrlCreateLabel("Password", 18, 51, 58, 17)
$Label1 = GUICtrlCreateLabel("IP P�blica", 18, 123, 62, 17)
$Label4 = GUICtrlCreateLabel("Host ", 18, 75, 34, 17)
$Label5 = GUICtrlCreateLabel("IP Actual", 18, 100, 56, 17)

GUICtrlCreateGroup("", -99, -99, 1, 1)
GUISetState(@SW_SHOW)

#EndRegion ### END Koda GUI section ###

;lleguim l'ini
GUICtrlSetData($input_UserDynDNS ,IniRead ( "dynDNS.ini", "credencials", "user", "no definit" ))
GUICtrlSetData($input_PassDynDNS ,IniRead ( "dynDNS.ini", "credencials", "pass", "" ))
GUICtrlSetData($input_HostDynDNS ,IniRead ( "dynDNS.ini", "credencials", "host", "no definit" ))

$IPPublica = _GetIP()

;Si tenim Host mirem la IP actual
If StringLen(GUICtrlRead($input_HostDynDNS)) > 0 Then
	$IPActual = TCPNameToIP(GUICtrlRead($input_HostDynDNS))
	GUICtrlSetData($Input_Actual ,$IPActual)
EndIf

If @error Then
    MsgBox(48, "Error", "No podem obtindre la IP P�blica, l'haur�s de posar a m�");
Else
	GUICtrlSetData($Input_IP ,$IPPublica)
EndIf

While 1
	Sleep(100)
WEnd

Func botoActualitzaClick()

	If GUICtrlRead($Input_Actual) <> GUICtrlRead($Input_IP) Then
		actualitzaDynDNS(GUICtrlRead($input_UserDynDNS),GUICtrlRead($input_PassDynDNS),GUICtrlRead($input_HostDynDNS),GUICtrlRead($Input_IP))

		;comprovem de nou la IP
		$IPActual = TCPNameToIP(GUICtrlRead($input_HostDynDNS))
		GUICtrlSetData($Input_Actual ,$IPActual)

	Else
		MsgBox(48, "Error", "La IP actual ja es la mateixa que la del DNS, no es pot actualitzar de nou" )
	EndIf

EndFunc

Func Form_PrincipalClose()
	Exit
EndFunc

Func Form_PrincipalMaximize()
EndFunc

Func Form_PrincipalMinimize()
	GUISetState(@SW_HIDE, $Form_Principal)
EndFunc

Func Form_PrincipalRestore()
	GUISetState(@SW_SHOW, $Form_Principal)
EndFunc

Func On_Click_TrayIcon()
	GUISetState(@SW_SHOW, $Form_Principal)
EndFunc





;---------------------------------------------------------
;
;---------------------------------------------------------
Func actualitzaDynDNS($Usuari,$Password,$Host,$NovaIP)

	Local $sLocalIP = "members.dyndns.org"

	; Inicialitzem
	Local $hOpen = _WinHttpOpen()

	; Agafem un conector
	Local $hConnect = _WinHttpConnect($hOpen, $sLocalIP)

	If @error Then
		MsgBox(48, "Error", "Error, no es pot conectar amb el servidor dyndns.org.")
		_WinHttpCloseHandle($hOpen)
		Exit
	EndIf

	Local $hRequest = _WinHttpOpenRequest($hConnect, _
        "GET", _ ; verb
        "/nic/update?system=dyndns&hostname=" & $Host & "&myip=" & $NovaIP, _ ; object
        Default, _ ; version
        Default, _ ; referrer
        "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") ; accept types


	; Posem les credencials
	_WinHttpSetCredentials($hRequest, $WINHTTP_AUTH_TARGET_SERVER, $WINHTTP_AUTH_SCHEME_BASIC, $Usuari, $Password)

	; Enviem peticio
	_WinHttpSendRequest($hRequest)

	If @error Then
		MsgBox(48, "Error", "Error enviant credencials")
		_WinHttpCloseHandle($hOpen)
	EndIf

	; Esperem resposta
	_WinHttpReceiveResponse($hRequest)

	; Mirem si hi ha info a llegir
	Local $sChunk, $sData
	If _WinHttpQueryDataAvailable($hRequest) Then
		; Read
		While 1
			$sChunk = _WinHttpReadData($hRequest)
			If @error Then ExitLoop
			$sData &= $sChunk
		WEnd

		Local $result
		$result = StringInStr($sData, "badauth")
		if $result > 0 Then
			MsgBox(48,"DynDNS Error","Usuari o password no v�lids")
		EndIf

		$result = StringInStr($sData, "nohost")
		if $result > 0 Then
			MsgBox(48,"DynDNS Error", "El Host '" & $Host & "' No existeix")
		EndIf

		$result = StringInStr($sData, "nochg")
		if $result > 0 Then
			MsgBox(64,"DynDNS", "La IP passada �s la mateixa que la que t� el Host, no s'actualitza")
		EndIf


		$result = StringInStr($sData, "good")
		if $result > 0 Then
			MsgBox(64,"DynDNS", "La IP " & $NovaIP & " s'ha actualitzat correctament")
		EndIf

		ConsoleWrite($sData & @CRLF)
	Else
		MsgBox(48, "DynDNS Error", "No podem conectar amb DynDNS")
	EndIf

	; tanquem
	_WinHttpCloseHandle($hRequest)
	_WinHttpCloseHandle($hConnect)
	_WinHttpCloseHandle($hOpen)

EndFunc
