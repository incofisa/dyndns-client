#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.6.1
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

#include "WinHttp.au3"

Opt("MustDeclareVars", 1)

; !!! The result of this script will be this sent to the server:
#cs
 POST /admin.php HTTP/1.1
 Connection: Keep-Alive
 Accept: text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8
 User-Agent: AutoIt/3.3
 Content-Length: 0
 Host: 127.0.0.1
 Authorization: Basic YWRtaW46YWRtaW4=
#ce

;http://members.easydns.com/dyn/dyndns.php?hostname=example.com&myip=10.0.0.2

; My server
Global $sLocalIP = "members.easydns.com"

; Initialize and get session handle
Global $hOpen = _WinHttpOpen()
; Get connection handle

Global $hConnect = _WinHttpConnect($hOpen, $sLocalIP)
; Specify the reguest


If @error Then
    MsgBox(48, "Error", "Error specifying the initial target server of an HTTP request.")
    _WinHttpCloseHandle($hOpen)
    Exit
EndIf

Global $hRequest = _WinHttpOpenRequest($hConnect, _
        "GET", _ ; verb
        "/dyn/dyndns.php?hostname=vila.calpaleta.com&myip=95.23.191.150", _ ; object
        Default, _ ; version
        Default, _ ; referrer
        "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") ; accept types



; Set credentials
_WinHttpSetCredentials($hRequest, $WINHTTP_AUTH_TARGET_SERVER, $WINHTTP_AUTH_SCHEME_BASIC, "simeo", "xxxxxxxxxxxx")





; Send request
_WinHttpSendRequest($hRequest)

If @error Then
    MsgBox(48, "Error", "Error enviant credencials")
    _WinHttpCloseHandle($hOpen)
    Exit
EndIf



; Wait for the response
_WinHttpReceiveResponse($hRequest)

; .... The rest of the code here...

; Close handles
_WinHttpCloseHandle($hRequest)
_WinHttpCloseHandle($hConnect)
_WinHttpCloseHandle($hOpen)
